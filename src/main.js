import './assets/main.css'

import { createApp } from 'vue';

import axios from "axios";
import router from "./router/index.js";
import App from "./App.vue"
import VueAxios from "vue-axios";

// const app = createApp(App);
//app.use(router, VueAxios, axios);

//createApp(App).mount('#app');



createApp(App)
    .use(VueAxios, axios)
    .use(router)
    .mount('#app')
