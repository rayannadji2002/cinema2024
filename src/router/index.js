import { createRouter, createWebHistory } from 'vue-router'
import Accueil from '@/views/Accueil.vue'
import RechercheFilms from '@/views/RechercheFilms.vue'
import Details from "@/views/Details.vue";


const routes = [
    {
        path: '/',
        name: 'Accueil',
        component: Accueil
    },
    {
        path: '/recherche',
        name: 'Recherche',
        component: RechercheFilms
    },
    {
        path: "/Details",
        name: "details",
        component: Details,
    }


]

const router = createRouter({
    history: createWebHistory(),
    routes
})

export default router
